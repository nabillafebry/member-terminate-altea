# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/member-terminate-altea.jar /member-terminate-altea.jar
# run application with this command line[
CMD ["java", "-jar", "/member-terminate-altea.jar"]
RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
