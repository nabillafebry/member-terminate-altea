package id.co.asyst.amala.terminate.altea.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Ahmad Dimas Abid Muttaqi
 * @version $Revision$, May 09, 2019
 * @since 1.0
 */

public class SimpleAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        return newExchange;
    }
}