package id.co.asyst.amala.terminate.altea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"terminate-altea.xml", "beans.xml"})
public class PasApp {

    public static void main(String[] args) {
        SpringApplication.run(PasApp.class, args);
    }
}