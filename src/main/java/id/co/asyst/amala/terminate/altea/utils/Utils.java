package id.co.asyst.amala.terminate.altea.utils;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Utils {
    static Logger LOG = LoggerFactory.getLogger(Utils.class);

    public void validateRequest(Exchange exchange) {
        Map<String, Object> requestdata = exchange.getProperty("requestdata", Map.class);

        String[] properties = new String[]{"startdate", "enddate", "updatedby"};
        String message = validate(requestdata, properties);
        if (message != null) {
            exchange.setProperty("resmsg", message);
            exchange.setProperty("rescode", "9005");
            exchange.setProperty("resdesc", "Invalid Data");
            return;
        }

        exchange.setProperty("startdate", requestdata.get("startdate"));
        exchange.setProperty("enddate", requestdata.get("enddate"));
        exchange.setProperty("updatedby", requestdata.get("updatedby"));

    }

    public void setLimit(Exchange exchange) {
        Map<String, Object> requestdata = exchange.getProperty("requestdata", Map.class);

        double limitdouble = (double) requestdata.get("limit");


        exchange.setProperty("limit", (int) limitdouble);
    }

    public void setCDCStructure(Exchange exchange) {
        Map<String, Object> member = exchange.getProperty("profile", Map.class);

        String memberid = member.get("memberid").toString();
        String status = member.get("status").toString();

        Map<String, Object> before = new HashMap<>();
        before.put("memberid", memberid);
        before.put("status", "ACTIVE");

        Map<String, Object> after = new HashMap<>();
        after.put("memberid", memberid);
//        after.put("status", status);
        after.put("status", "TERMINATED");

        Map<String, Object> source = new HashMap<>();
        source.put("table", "member");

        Map<String, Object> mapstructure = new HashMap<>();
        mapstructure.put("before", before);
        mapstructure.put("after", after);
        mapstructure.put("source", source);

        Gson gson = new Gson();

        exchange.getOut().setBody(gson.toJson(mapstructure));
    }


    private String validate(Map<String, Object> data, String property) {
        if (data.get(property) == null || data.get(property).toString().equals(""))
            return property + " is required";
        return null;
    }

    private String validate(Map<String, Object> data, String[] properties) {
        for (String property : properties) {
            if (data.get(property) == null || data.get(property).toString().equals("")) {
                return property + " is required";
            }


        }
        return null;
    }


}
