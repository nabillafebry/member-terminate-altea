package id.co.asyst.amala.terminate.altea.utils;

import com.google.gson.Gson;
import id.co.asyst.commons.core.payload.Status;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ConstructResponse {
    public void constructExceptionResponse(Exchange exchange) {
        Map<String, Object> response = new HashMap<>();
        Map<String, Object> identity = new HashMap<String, Object>();


        identity.put("reqtxnid", exchange.getProperty("reqtxnid"));
        identity.put("reqdate", exchange.getProperty("reqdate"));
        identity.put("appid", exchange.getProperty("appid"));
        identity.put("userid", exchange.getProperty("userid"));
        identity.put("signature", exchange.getProperty("signature"));
        identity.put("seqno", exchange.getProperty("seqno"));

        Status status;
        Message in = exchange.getIn();
        System.out.println("resmessage :: " + in.getHeader("resmessage"));
        if (!StringUtils.isEmpty(in.getHeader("resmessage"))) {
            String resmessage = in.getHeader("resmessage").toString();
            if (resmessage.contains("is required")) {
                StringBuilder sb = new StringBuilder();
                if (resmessage.contains("apptxnid")) {
                    sb.append("apptxnid");
                }
                if (resmessage.contains("signature")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", signature");
                    } else {
                        sb.append(" signature");
                    }
                }
                if (resmessage.contains("appid")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", appid");
                    } else {
                        sb.append(" appid");
                    }
                }
                if (resmessage.contains("reqdate")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", reqdate");
                    } else {
                        sb.append(" reqdate");
                    }
                }
                if (resmessage.contains("userid")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", userid");
                    } else {
                        sb.append(" userid");
                    }
                }
                if (resmessage.contains("reqtxnid")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", reqtxnid");
                    } else {
                        sb.append(" reqtxnid");
                    }
                }
                if (resmessage.contains("seqno")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", seqno");
                    } else {
                        sb.append(" seqno");
                    }
                }
                if (resmessage.contains("startdate")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", startdate");
                    } else {
                        sb.append(" startdate");
                    }
                }
                if (resmessage.contains("enddate")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", enddate");
                    } else {
                        sb.append(" enddate");
                    }
                }
                if (resmessage.contains("updatedby")) {
                    if (!StringUtils.isEmpty(sb.toString())) {
                        sb.append(", updatedby");
                    } else {
                        sb.append(" updatedby");
                    }
                }

                sb.append(" is required");
                status = Status.INVALID(sb.toString());
            } else {
                status = Status.APP_ERROR(in.getHeader("resmessage").toString());
            }
        } else {
            status = Status.APP_ERROR();
        }

//        response.put("identity", constructIdentity(exchange));
        response.put("status", status);
        response.put("identity", identity);

        exchange.getOut().setBody(new Gson().toJson(response));
    }

    public void constructDataNotFoundResponse(Exchange exchange) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Map<String, Object> identity = new HashMap<String, Object>();


        identity.put("reqtxnid", exchange.getProperty("reqtxnid"));
        identity.put("reqdate", exchange.getProperty("reqdate"));
        identity.put("appid", exchange.getProperty("appid"));
        identity.put("userid", exchange.getProperty("userid"));
        identity.put("signature", exchange.getProperty("signature"));
        identity.put("seqno", exchange.getProperty("seqno"));


        status.put("responsecode", "9003");
        status.put("responsedesc", "Data Not Found");

        String responsemessage = exchange.getProperty("resmsg", String.class);

        status.put("responsemessage", responsemessage);

        response.put("status", status);
        response.put("identity", identity);
        response.put("result", "");


        Gson gson = new Gson();
        exchange.getOut().setBody(gson.toJson(response));
    }

    public void constructInvalidResponse(Exchange exchange) {
        Map<String, Object> response = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Map<String, Object> identity = new HashMap<String, Object>();


        identity.put("reqtxnid", exchange.getProperty("reqtxnid"));
        identity.put("reqdate", exchange.getProperty("reqdate"));
        identity.put("appid", exchange.getProperty("appid"));
        identity.put("userid", exchange.getProperty("userid"));
        identity.put("signature", exchange.getProperty("signature"));
        identity.put("seqno", exchange.getProperty("seqno"));


        String responsemessage = exchange.getProperty("resmsg", String.class);
        String responsedesc = exchange.getProperty("resdesc", String.class);
        String responsecode = exchange.getProperty("rescode", String.class);

        status.put("responsemessage", responsemessage);
        status.put("responsedesc", responsedesc);
        status.put("responsecode", responsecode);


        response.put("status", status);
        response.put("identity", identity);


        Gson gson = new Gson();
        exchange.getOut().setBody(gson.toJson(response));
    }
}
