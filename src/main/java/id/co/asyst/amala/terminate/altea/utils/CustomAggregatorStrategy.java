package id.co.asyst.amala.terminate.altea.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Feb 14, 2019
 * @since 1.0
 */
public class CustomAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
//        List<Map<String,Object>> spending = newExchange.getProperty("piecesspending", List.class);
//        List<Map<String,Object>> spendingList;
//
        if (oldExchange != null)
            newExchange.setProperty("headerHandback", oldExchange.getProperty("headerHandback"));
//        else {
//            spendingList = oldExchange.getProperty("spending", List.class);
//            spendingList.addAll(spending);
//        }
//
        return newExchange;
    }
}
